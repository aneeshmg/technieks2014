<?php
include 'visitorcounter.php';

function ActorCarousel() {
  echo "<div class=\"actors\"
  style=\"position:absolute;top:20px;left:30px;opacity:.9;background-color:#000;\">";
  for($i = 1; $i <= 10; $i++) {
    echo "<img src=\"images/gal" . (mt_rand(100,1000)) % 18 . ".jpg\" />";
  }
  echo "</div>";
}
?>-
<!DOCTYPE html>
<html>
  <head>
    <title>techNIEks 2014 | Day 3</title>
      <meta charset="utf-8" />
      <meta name="Description" content="National level technocultural fest">
      <meta name="Keywords" content="youth, fest, annual, technocultural">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <link rel="shortcut icon" href="images/logo.png" type="image/png">
      <!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
      <script src="js/jquery.js" type="text/javascript"></script>
      <script>window.jQuery || document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><\/script>')</script>
      <script type="text/javascript" src="js/jquery.ui.core.min.js"></script>
      <script type="text/javascript" src="js/jquery.ui.widget.min.js"></script>
      <script type="text/javascript" src="js/jquery.ui.rcarousel.min.js"></script>
      <script>
        $(function() {
          $('.actors').rcarousel( {
            orientation: "vertical",
            auto:  {
              enabled: true,
              interval: 0
            },
            width:115,
            height:100,
            visible:6,
            speed:10000,
            step:1
          });
        });

      </script>
    <style>
      @import url(http://fonts.googleapis.com/css?family=Text+Me+One);
      @import url(http://fonts.googleapis.com/css?family=Varela+Round);
      body{
        background-image: url('images/bg.jpg');
      }
      #header {
        position: fixed center;
        height: 80px;
        margin: auto;
        width: 900px;
        padding-top: 10px;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
      }
      #logo-img {
        height: 70px;
        width: 60px;
        position: relative;
        top: -15px;
        right:290px;
      }
      #header-technieks {
        color: #fff;
        position: relative;
        top: -115px;
        font-family: 'Text Me One', sans-serif;
      }
      #header-technieks img {
        position: relative;
        top:0px;
      }
      #header-technieks b {
        color: #f00;
        font-size: 18px;
      }
      #header-colg {
        position: relative;
        bottom:95px;  
        font-family: 'Text Me One', sans-serif;
        color: #fff;
        font-size: 20px;
      }
	  .eventBtn {
	    padding: 5px 15px 5px 15px;
		  font-size: 20px;
		  opacity: 0.9;
		  transition: .2s;
		  text-decoration: none;
		  position: relative;
		  top: 90px;
		  width: 100px;
		  font-family: 'text me one', 'sans serif';
		  font-weight: bold;
		  margin-left: 35px;
		  display: inline;
	  }
	  .tech {
	    color: #fff;
		  background-color: #DF0101;
	  }
	  .tech:hover {
	    color: #fff;
		  background-color: #DF0101;
		  opacity: 1;
		  text-decoration: none;
		  transition: 0.2s;
	  }
	  a:link {
	    text-decoration: none;
	  }
	  a:hover {
	    text-decoration: none;
	  }
	  .eventBox1{
	    float: left;
		  margin: 10px 10px 10px 10px;
      font-family: 'Text Me One', 'Sans serif';
		  position: relative;
		  left: 12%;
		  right: 20%;
		  top: 110px;
		  overflow: hidden;
		  width: 300px;
		  height: 200px;;
		  opacity: 0.9;
		  font-weight: bold;
		  padding: 5px 10px 5px 10px;
    }
	  .color1 {
      background-color: #DF0101;
		  color: #f3f3f3;
	  }
	  .color2 {
      background-color: #f3f3f3;
		  color: #000;
	  }
	  .eventBox1:hover {
	    opacity: 1;
		  transition: 0.2s;
		  cursor: pointer;
	  }
	  #day {
	    font-weight: bold;
		  color: #f3f3f3;
		  position: relative;
		  top: 110px;
		  font-family: 'Varela Round', 'sans serif';
	  }
    </style>
  </head>
  <body>
    <center>
    <div id="header">
      <img src="images/logo.png" id="logo-img"/>
      <h2 id="header-colg">The National Institute of Engineering, Mysore presents</h2>
      <div id="header-technieks">
        <img src="images/techlogo2.png" height="70px" width="370px"/><br>
        "Celebrating 100 years of Indian cinema"<br><b>13th, 14th and 15th March</b>
      </div>
    </div>
    </center>
	<center>
	  <div class = "eventBtn tech" style = "cursor: pointer"><a href = "day1.php">Day 1</a></div>
	  <div class = "eventBtn tech" style = "cursor: pointer"><a href = "day2.php">Day 2</a></div>
	  <br><br>
	  <div id = "day">DAY THREE EVENTS</div>
	  <br>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Friends Forever<!--/a--></b><br>
	    How well do you know your friend???</br>Details: At 10:30 am in GJB Room 210.</br>Fee: Rs 40 for a team of 2
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Battle Field<!--/a--></b><br>
	   Virtual Shooting game.</br>Details: At 11:30 am in GJB 3rd floor.</br>Fee: Rs 60 per person and Rs 240 for a team of 4.
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Sports Quiz<!--/a--></b><br>
	  To test your knowledge about sports.</br>Details: At 10:30 am in Sir M V Hall.</br>Fee: Rs 30
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Between the nets<!--/a--></b><br>
	   Throw the ball in between the nets.</br>Details: At 11:00 am in GJB Ground.</br>Fee: Rs 50 for a team of 4
	   </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Blind Track<!--/a--></b><br>
	    Can you guide your friend in times of darkness?</br>Details: At 11:00 am in Parking lot.</br>Fee: Rs 40 for a team of 
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Pay And Play<!--/a--></b><br>
	    Pay and test your basketball shooting skills.</br>Details: At 11:30 am in Basketball Ground.</br>Fee: Rs 10
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Task in 2 Minutes<!--/a--></b><br>
	    Chance to be a winner in 2 minutes.</br>Details: At 12:00 pm in GJB Room 213.</br>Fee: Rs 30
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Chowkabara<!--/a--></b></p>
	    Classical traditional game.</br>Details: At 12:00 pm in Canteen Hut.</br>Fee: Rs 10
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Tug Of War<!--/a--></b><br>
	     Watch the hunk come face to face in this arm wrenching event.</br>Details: At 12:00 pm in Throwball Court.</br>Fee: Rs 150 per team of 5
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Rope Climbing<!--/a--></b><br>
	     Do you think you can beat the wild... Then here is your chance to prove it.</br>Details: At 02:00 pm in Sports Complex.</br>Fee: Rs 20 for single and Rs 80 for relay
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->General Quiz<!--/a--></b><br>
	     Quiz to test your general knowledge.</br>Details: At 02:00 pm in Sir M V Hall.</br>Fee: Rs 30
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Lagori<!--/a--></b><br>
	     Simple game of lagori.</br>Details: At 02:00 pm in Parking Lot.</br>Fee: Rs 150 per team of 7
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Murder In The Jungle<!--/a--></b><br>
	    Investigating a Crime Scene on campus, to test your skills.</br>Details: At 02:30 pm in GJB.</br>Fee: Rs 40
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Slow Bike Race<!--/a--></b><br>
	     It's all about how slow you ride your bike.</br>Details: At 02:30 pm in Parking Lot.</br>Fee: Rs 30
	  </div>
	</center>

    <? ActorCarousel(); ?>>
  </body>
</html>
