$(document).ready(function() {
  console.log("jquery loaded");
  $('#container').fullContent({
		    stages: 'div',
		    mapPosition: [{v: 1, h: 3}, {v: 2, h: 1}, {v: 3, h: 2}, {v: 3, h: 4}, {v: 2, h: 5}, {v: 5, h: 5}],
		    stageStart: 1,
        speedTransition: 800,
		    idComplement: 'page_'
  });
  $(function() {
          $('.actors').rcarousel( {
            orientation: "vertical",
            auto:  {
              enabled: true,
              interval: 0
            },
            width:115,
            height:100,
            visible:6,
            speed:10000,
            step:1
          });
        });
  $(function() {
    $('#updates-roll').rcarousel( {
      auto: {
        enabled: true,
        interval: 4000
      },
      width:900,
      height:320,
      visible:1,
      speed:400,
      step:1
    });
  });
  $(function() {
    $('#partners').rcarousel( {
      auto: {
        enabled: true,
        interval: 4000
      },
      width:150,
      height:150,
      visible:1,
      speed:400,
      step:1
    });
  });
  $('#clockdiv').html('<object data="./clock" width="1000px" height="300px">');
  $.get('android/data/update.txt', function(data) {
    $('#updates').append('<b>Updates:</b><br>' + data);
  });
  $.get('data/tweetofday.txt', function(data) {
    $('#tweet').append('<b>Tweet of the day:</b><br>' + data);
  });
  $.get('data/visitcount.txt', function(data) {
    $('#visitorcount').append(data);
  });
  $('.nav-about').click(function() {
    $('title').html("techNIEks 2014 | About");
  });
  $('.nav-contact').click(function() {
    $('title').html("techNIEks 2014 | Contact");
  });
  $('.nav-home').click(function() {
    $('title').html("techNIEks 2014 | Home");
  });
  $('.nav-sponsors').click(function() {
    $('title').html("techNIEks 2014 | Sponsors");
  });
  $('.nav-events').click(function() {
    $('title').html("techNIEks 2014 | Events");
  });
  $('.nav-gallery').click(function() {
    $('title').html("techNIEks 2014 | Gallery");
  });
  $('#rulespp').click(function() {
    window.location.href="PicturePerfect-rules.html";
    return false;
  });
  $('#registerpp').click(function() {
    window.location.href="http://goo.gl/hu2GtO";
    return false;
  });



});
