<?php
include 'visitorcounter.php';
include 'redirect.php';

function ActorCarousel() {
  echo "<a href=\"#gallery\"><div class=\"actors\"
  style=\"position:absolute;top:20px;left:30px;opacity:.9;background-color:#000;\">";
  for($i = 1; $i <= 10; $i++) {
    echo "<img src=\"images/gal" . ((mt_rand(100,1000) % 17)+1) . ".jpg\" />";
  }
  echo "</div></a>";
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>techNIEks 2014</title>
    <!-- Meta tags -->
      <meta charset="utf-8" />
      <meta name="Description" content="National level technocultural fest">
      <meta name="Keywords" content="youth, fest, annual, technocultural">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Metas End -->
    <!-- CSS -->
      <link rel="shortcut icon" href="images/logo.png" type="image/png">
      <link  href="http://fotorama.s3.amazonaws.com/4.4.8/fotorama.css" rel="stylesheet">
      <!--link type="text/css" rel="stylesheet" href="css/rcarousel.css" /-->
      <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- CSS Ends -->
    <!-- Scripts -->
      <!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
      <script src="js/jquery.js" type="text/javascript"></script>
      <script>window.jQuery || document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><\/script>')</script>
      <script src="js/jquery.scrollTo.js" type="text/javascript"></script>
      <script src="js/jquery.fullContent.js" type="text/javascript"></script>
      <script src="http://fotorama.s3.amazonaws.com/4.4.8/fotorama.js"></script>
      <script type="text/javascript" src="js/jquery.ui.core.min.js"></script>
      <script type="text/javascript" src="js/jquery.ui.widget.min.js"></script>
      <script type="text/javascript" src="js/jquery.ui.rcarousel.min.js"></script>
      <script src="js/script.js" type="text/javascript"></script>

    <!-- Scripts End -->
  </head>
  <body>

    <div id="container">

      <div id="home" class="home scrollv">

        <div class="wrap">
          <center>
          <div id="header">
            <img src="images/logo.png" id="logo-img"/>
            <h2 id="header-colg">The National Institute of Engineering, Mysore presents</h2>
            <div id="header-technieks">
              <img src="images/techlogo2.png" height="70px" width="370px"/><br>
              "Celebrating 100 years of Indian cinema"<br><b>13th, 14th and 15th March</b>
            </div>
          </div>
          </center>
          <ul class="nav">
            <li><a href="#about" class="nav-about">About</a></li>
            <li><a href="#contact" class="nav-contact">Contact</a></li>
            <li><a href="#events" class="nav-events">Events</a></li>
            <li><a href="#sponsors" class="nav-sponsors">Sponsors</a></li>
            <li><a href="#gallery" class="nav-gaallery">Gallery</a></li>
          </ul>
          
            <?php ActorCarousel(); ?>
          
          <div id="clockdiv"></div>
          <center><div id="updates-roll">
            <img src="images/up1.jpg">
            <img src="images/up2.jpg">
            <img src="images/up3.jpg">
            <img src="images/up5.jpg">
          </div>  </center>
          <a href="https://twitter.com/technieks2014" class="twitter-follow-button" data-show-count="false">Follow @technieks2014</a>
          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
          <iframe
          src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FtechNIEks&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=80&amp;appId=169496736592964"
          scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:80px;"
          allowTransparency="true" id="fb-like"></iframe>
          <div id="events-nav">
            <ul>
              <li><a href="day1.php">Day 1</a></li>
              <li><a href="day2.php">Day 2</a></li>
              <li><a href="day3.php">Day 3</a></li>
            </ul>
          </div>
          <span id="partners-text">Our Partners</span>
          <div id="partners">
            <img src="images/partners/redfm.jpg" />
            <img src="images/partners/deccan.jpg" />
            <img src="images/partners/som.jpg" />
            <img src="images/partners/amogh.jpg" />
            <img src="images/partners/jade.jpg" />
            <img src="images/partners/city.jpg" />
            <img src="images/partners/travel.jpg" />
            <img src="images/partners/adsindia.jpg" />
            <img src="images/partners/graviti.jpg" />
            <img src="images/sponsors/hospital.jpg" />
          </div>
          <div id="updates"></div>
          <div id="tweet"></div>
          <div id="visitorcount">Visitors: </div>
        </div>
          <div id="footer">&copy; techNIEks 2014</div>

      </div>
      <!-- Home div ends -->
      <div id="about" class="about scrollv">

        <div class="wrap">
          <center>
          <div id="header">
            <img src="images/logo.png" id="logo-img"/>
            <h2 id="header-colg">The National Institute of Engineering, Mysore presents</h2>
            <div id="header-technieks">
              <img src="images/techlogo2.png" height="70px" width="370px"/><br>
              "Celebrating 100 years of Indian cinema"<br><b>13th, 14th and 15th March</b>
            </div>
          </div>
          </center>
          <ul class="nav">
            <li><a href="#home" class="nav-home">Home</a></li>
            <li><a href="#contact" class="nav-contact">Contact</a></li>
            <li><a href="#events" class="nav-events">Events</a></li>
            <li><a href="#sponsors" class="nav-sponsors">Sponsors</a></li>
            <li><a href="#gallery" class="nav-gallery">Gallery</a></li>
          </ul>
          
            <?php ActorCarousel(); ?>
          
          <center><div id="descofcollege">
            <a href="http://www.nie.ac.in/" target="_blank"><img src="images/nie-pic.jpg"
            height="200px" width="350px" /><<br>
            <b>The National Institute of Engineering</b>, Mysore, is amongst the premier engineering
            colleges in India and offers degrees in undergraduate, postgraduate and doctoral
            studies. It also provides certificate postgraduate diploma courses, and is a well-known
            synonym for quality education and professionalism all over the country.</a>
          </div>
          <div id="descoftechnieks">
            <a href="#home"><img src="images/technieks-pic.jpg" height="200px" width="350px"></a><br>
            <b>techNIEks</b>, the annual techno-cultural extravaganza, organized by the
            National Institute of Engineering, Mysore, is scheduled to be held from the 13th to 15th
            March 2014, based on the theme "100 Years of Indian Cinema". techNIEks has been a grand
            success over the years, bringing together the youth from many institutes all over India.
          </div></center>
          <!--div id="aboutus"></div-->
          <div id="socialmedia">
            <a target="_blank" href="https://www.facebook.com/techNIEks"><img class="fbtw" src="images/facebook.png" /></a>
            <a target="_blank" href="#home"><img class="fbtw" src="images/twitter.png" /></a>
            <a target="_blank" href="android/download.php" title="Download the app!"><img class="fbtw" id="android-download" src="images/App-icon.png" /></a>
          </div>
        </div>
          <div id="footer">&copy; techNIEks 2014</div>

      </div>
      <!-- About div ends -->
      <div id="events" class="events scrollv">

        <div class="wrap">
          <center>
          <div id="header">
            <img src="images/logo.png" id="logo-img"/>
            <h2 id="header-colg">The National Institute of Engineering, Mysore presents</h2>
            <div id="header-technieks">
              <img src="images/techlogo2.png" height="70px" width="370px"/><br>
              "Celebrating 100 years of Indian cinema"<br><b>13th, 14th and 15th March</b>
            </div>
          </div>
          </center>
          <ul class="nav">
            <li><a href="#home" class="nav-home">Home</a></li>
            <li><a href="#about" class="nav-about">About</a></li>
            <li><a href="#contact" class="nav-contact">Contact</a></li>
            <li><a href="#sponsors" class="nav-sponsors">Sponsors</a></li>
            <li><a href="#gallery" class="nav-gallery">Gallery</a></li>
          </ul>
          
            <?php ActorCarousel(); ?>
          
          <center><div
          style="margin-top:-20px;font-family:'Varela Round';font-size:20px;"><b><a
          href="pre-techNIEks-events.html" target="_blank" class="btn red">Pre-techNIEks
          events</a></b></div><br><br>
          <center><div style="color:#eee;margin-top:-20px;font-family:'Varela Round';font-size:20px;"><b><u>Signature events</u></b></div></center><br>
          <center><div id="events-box">
            
            <div class="event-desc">
              <a href="https://www.facebook.com/events/1504086253151402/?ref=22" title="Click to know more">
              <img src="images/tiles/lagori.jpg" height="280px" width="268px" />
              <div class="desc">
                <b style="color:#f00;font-size:20px;">Band performance by</b><br>Lagori - Inspired
                by the popular south Indian game of Lagori, this band livens up the party with the
                fun and energy that comes along with the game.
              </div>
              </a>
            </div>
            <div class="event-desc">
              <a href="https://www.facebook.com/events/668218493217556/" title="Click to know more">
              <img src="images/tiles/marathon.jpg" height="280px" width="268px" />
              <div class="desc">
                <b style="color:#f00;font-size:20px;">Marathon</b><br>If you have a cause namma
                MYSOORU, will run for it...!
                One of the biggest event of techNIEks is back! 10k RUN
                With a social cause-WOMEN SAFETY
              </div>
              </a>
            </div>
            <div class="event-desc">
              <a href="#" title="Click to know more">
              <img src="images/tiles/glowart.jpg" height="280px" width="268px" />
              <div class="desc">
                <b style="color:#f00;font-size:20px;">Glow Art</b><br>If you think light is a source
                of life, he uses darkness to create amazing art and evolve wonders through that
                magic light and canvas.
              </div>
              </a>
            </div>
            <div class="event-desc">
              <a href="https://www.facebook.com/events/652670488102703/" title="Click to know more">
              <img src="images/tiles/rockarena.jpg" height="280px" width="268px" />
              <div class="desc">
                <b style="color:#f00;font-size:20px;">Rock Arena</b><br>techNIEks 14 will showcase
                fresh talent of leading youth bands of India performing for the crowds.
              </div>
              </a>
            </div>
            <div class="event-desc">
              <a href="#" title="Click to know more">
              <img src="images/tiles/silhoulet.jpg" height="280px" width="268px" />
              <div class="desc">
                <b style="color:#f00;font-size:20px;">Silhouette Dance</b><br>A dance with elements
                of light and shadow. With perfect body language, their moves make the story even
                more intriguing. With no fancy costumes, to take people's attention off their
                technique, they are by far the most talented performers.
              </div>
              </a>
            </div>
            <div class="event-desc">
              <a href="#" title="Click to know more">
              <img src="images/tiles/fashion.jpg" height="280px" width="268px" />
              <div class="desc">
                <b style="color:#f00;font-size:20px;">Fashion Show</b><br>Another signature event of
                techNIEks 2014, the Fashion Show will be based on specialized themes, ranging from
                the recent trends in fashion to the classy and elegant.
              </div>
              </a>
            </div>
            <div class="event-desc">
              <a href="#" title="Click to know more">
              <img src="images/tiles/pictureperfect.jpg" height="280px" width="268px" />
              <div class="desc">
                <b style="color:#f00;font-size:20px;">Picture Perfect</b><br>A competition in which young directors, actors, screenplay writers, editors, 
              cinematographers, singers, story writers etc. get a chance to show the stories that they have shot through this 
              competition and gain wide recognition and apprecoation for their work.<br><br>
              <span id="rulespp" class="btn red">Rules</span>
              <span id="registerpp" class="btn red">Register</span>
              </div>
              </a>
            </div>
            <div class="event-desc">
              <a href="https://www.facebook.com/events/726066490759599/?ref=3" title="Click to know more">
              <img src="images/tiles/sangeet.jpg" height="280px" width="268px" />
              <div class="desc">
                <b style="color:#f00;font-size:20px;">Sangeet Shatrang</b><br>A state level carnatic
                music contest organised as a part of technieks 2014,commemorating hundreds of
                quality musicians who have contributed to music.
              </div>
              </a>
            </div>
            <div class="event-desc">
              <a href="https://www.facebook.com/events/366478983494398/?ref=22" title="Click to know more">
              <img src="images/tiles/photomania.jpg" height="280px" width="268px" />
              <div class="desc">
                <b style="color:#f00;font-size:20px;">Photomania</b><br>Love Photography? Is
                photography more than just a hobby to you? Here is your chance to showcase your
                skill. Showcase your art at Photomania. Submit your entries to techNIEks presents
                "Photomania" and win exciting prizes. <br> Categories: <br>Camera- Life in Mysore &
                celebration<br>Smartphone- Landscape
              </div>
              </a>
            </div>

          </div></center>

        </div>
          <div id="footer">&copy; techNIEks 2014</div>

      </div>
      <!-- Events div ends -->
      <div id="contact" class="contact">

        <div class="wrap">
          <center>
          <div id="header">
            <img src="images/logo.png" id="logo-img"/>
            <h2 id="header-colg">The National Institute of Engineering, Mysore presents</h2>
            <div id="header-technieks">
              <img src="images/techlogo2.png" height="70px" width="370px"/><br>
              "Celebrating 100 years of Indian cinema"<br><b>13th, 14th and 15th March</b>
            </div>
          </div>
          </center>
          <ul class="nav">
            <li><a href="#home" class="nav-home">Home</a></li>
            <li><a href="#about" class="nav-about">About</a></li>
            <li><a href="#events" class="nav-events">Events</a></li>
            <li><a href="#sponsors" class="nav-sponsors">Sponsors</a></li>
            <li><a href="#gallery" class="nav-gallery">Gallery</a></li>
          </ul>
          
            <?php ActorCarousel(); ?>
          
          <div id="contact-info">
            <b>Pavan Nayak [Student Co-ordinator]</b><br />+91 90369 20654 <br /><br />
            <b>Nimisha Nandakumar [General Secretary]</b><br />+91 98804 30815 <br /><br/>
            <b>Amit Nandan P [General Secretary]</b><br />+91 99806 10060 <br /><br />
            e-mail: <i> technieks2014@gmail.com</i>
          </div>
        </div>
          <div id="footer">&copy; techNIEks 2014</div>

      </div>
      <!-- Events div ends -->
      <div id="sponsors" class="sponsors scrollv">

        <div class="wrap">
          <center>
          <div id="header">
            <img src="images/logo.png" id="logo-img"/>
            <h2 id="header-colg">The National Institute of Engineering, Mysore presents</h2>
            <div id="header-technieks">
              <img src="images/techlogo2.png" height="70px" width="370px"/><br>
              "Celebrating 100 years of Indian cinema"<br><b>13th, 14th and 15th March</b>
            </div>
          </div>
          </center>
          <ul class="nav">
            <li><a href="#home" class="nav-home">Home</a></li>
            <li><a href="#about" class="nav-about">About</a></li>
            <li><a href="#events" class="nav-events">Events</a></li>
            <li><a href="#contact" class="nav-contact">Contact</a></li>
            <li><a href="#gallery" class="nav-gallery">Gallery</a></li>
          </ul>
          
            <?php ActorCarousel(); ?>
          
          <center><div id="sponsors-box">
              <div class="sponsor-desc">Platinum Sponsors</div>
              <a href="#"><img src="images/sponsors/myra.jpg" width="350px" height="200px" /></a>
              <a href="#"><img src="images/sponsors/tcs.jpg" width="350px" height="200px" /></a>
              <div class="sponsor-desc">Diamond Sponsor</div>
              <a href="#"><img src="images/sponsors/tvs.jpg" width="300px" height="150px" /></a><br>
              <div class="sponsor-desc">Gold Sponsors</div>
              <a href="#"><img src="images/sponsors/pgshetty.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/emarath.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/naf.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/ganesh.gif" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/gca.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/sarovara.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/bizworld.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/cycle.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/pakva.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/eventura.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/snap.gif" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/saiganesh.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/sweets.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/vastava.png" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/hospital.jpg" width="250px" height="100px" /></a>
              <a href="#"><img src="images/sponsors/gss.jpg" width="250px" height="100px" /></a><br>
              <div class="sponsors-imgless"><a href="#">Gurukrupa Caterers</a></div><br><br><p></p><br>



              <a href="#"><img src="images/sponsors/casa.jpg" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/golds.gif" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/rlconst.gif" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/paramur.jpg" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/indianestate.jpg" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/ceramic.jpg" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/kajaria.jpg" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/hsil.jpg" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/orion.jpg" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/sharp.jpg" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/decibells.jpg" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/nalpak.jpg" width="200px" height="80px" /></a>
              <a href="#"><img src="images/sponsors/arjun.jpg" width="200px" height="80px" /></a>
              <div class="sponsors-imgless">
                <a href="">Siddeshwara Agro Mill</a><br>
                <a href="#">Nagesh</a><br>
                <a href="#">CRC NIE</a><br>
                <a href="#">Sonia Motors</a><br>
                <a href="#">Shreya Batteries</a><br>
                <a href="#">Nanda Constructions</a><br>
                <a href="#">MN Agro Industries</a><br>
                <a href="#">Akruthi Home Gallery</a><br>
                <a href="#">Raghuveer Agencies</a><br>
              </div>

          </div></center>
        </div>
          <div id="footer" style="position:static;bottom:2%px;">&copy; techNIEks 2014</div>

      </div>
      <!-- Sponsors div ends -->
      <div id="gallery" class="gallery scrollv">

        <div class="wrap">
          <center>
          <ul class="nav" style="-webkit-transform: scale(0.6);-moz-transform:
          scale(0.6);margin-bottom:-80px;top:-27px;">
            <li><a href="#home" class="nav-home">Home</a></li>
            <li><a href="#about" class="nav-about">About</a></li>
            <li><a href="#events" class="nav-events">Events</a></li>
            <li><a href="#sponsors" class="nav-sponsors">Sponsors</a></li>
          </ul>
          <div class="fotorama" style="-webkit-transform: scale(0.9);-moz-transform:scale(0.9);"
          data-allowfullscreen="true" data-nav="thumbs" data-shuffle="true" data-autoplay="true">
            <a href="images/gal1.jpg"></a>
            <a href="images/gal2.jpg"></a>
            <a href="images/gal3.jpg"></a>
            <a href="images/gal5.jpg"></a>
            <a href="images/gal6.jpg"></a>
            <a href="images/gal7.jpg"></a>
            <a href="images/gal8.jpg"></a>
            <a href="images/gal9.jpg"></a>
            <a href="images/gal10.jpg"></a>
            <a href="images/gal11.jpg"></a>
            <a href="images/gal12.jpg"></a>
            <a href="images/gal13.jpg"></a>
            <a href="images/gal14.jpg"></a>
            <a href="images/gal15.jpg"></a>
            <a href="images/gal16.jpg"></a>
            <a href="images/gal17.jpg"></a>
            <a href="images/gal18.jpg"></a>
          </div>
          </center>
        </div>

      </div>
      <!-- Gallery div ends -->
    </div>
    <!-- Container div ends -->
  </body>

</html>
