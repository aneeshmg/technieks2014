
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <title>techNIEks Clock</title>
        <link rel="stylesheet" href="nieclock.css" type="text/css" media="all" />
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><\/script>')</script>
        <script type="text/javascript" src="nieclock.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                techNIEksCountdown({
                    secondsColor : "#df0101",
                    secondsGlow  : "none",
                    minutesColor : "#df0101",
                    minutesGlow  : "none",
                    hoursColor   : "#df0101",
                    hoursGlow    : "none",
                    daysColor    : "#df0101",
                    daysGlow     : "none",
                    startDate   : "1390199400",
                    endDate     : "1394685000",
                    now         : <?php echo '"'+time()+'"';?>
                });
                
            });
        </script>
    </head>
    <body bgcolor="grey">
        <div class="wrapper">
        <!--h4>We will be Live in: </h4-->
        <div class="clock">
            <div class="clock_days">
                <div class="bgLayer">
                    <div class="topLayer"></div>
                    <canvas id="canvas_days" width="188" height="188">
                    </canvas>
                    <div class="text">
                        <p class="val">0</p>
                        <p class="type_days">Days</p>
                    </div>
                </div>
            </div>
            <div class="clock_hours">
                <div class="bgLayer">
                    <div class="topLayer"></div>
                    <canvas id="canvas_hours" width="188" height="188">
                    </canvas>
                    <div class="text">
                        <p class="val">0</p>
                        <p class="type_hours">Hours</p>
                    </div>
                </div>
            </div>
            <div class="clock_minutes">
                <div class="bgLayer">
                    <div class="topLayer"></div>
                    <canvas id="canvas_minutes" width="188" height="188">
                    </canvas>
                    <div class="text">
                        <p class="val">0</p>
                        <p class="type_minutes">Minutes</p>
                    </div>
                </div>
            </div>
            <div class="clock_seconds">
                <div class="bgLayer">
                    <div class="topLayer"></div>
                    <canvas id="canvas_seconds" width="188" height="188">
                    </canvas>
                    <div class="text">
                        <p class="val">0</p>
                        <p class="type_seconds">Seconds</p>
                    </div>
                </div>
            </div>
        </div>
        <!--div class="divider">
       	  <p>In the meantime why dont follow us on <a href="#">Twitter?</a></p>
        </div>
        </div--><!--/wrapper-->
    </body>
</html>
