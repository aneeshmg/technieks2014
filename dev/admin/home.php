<?php
session_start();
if(!isset($_SESSION['auth'])) {
  header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>technieks 2014 Admin</title>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <center>
      <div class="nav">
        <ul id="login-box">
          <form action="logout.php" method="post">
            <li><div id="header">techNIEks 2014 Admin page</div></li>
            <li><input type="submit" value="Logout" id="enter"></li>
          </form>
        </ul>
      </div>
      <div class="wrapper">
        <div class="wrapper-text">Updates</div>
        <textarea id="updates" rows="4" cols="80"></textarea><br>
        <button id="new-update" class="button">Update</button>
      </div>
      <div class="wrapper">
        <div class="wrapper-text">Tweet of the day</div>
        <textarea id="tweets" rows="4" cols="80"></textarea><br>
        <button id="new-tweet" class="button">Save</button>
      </div>
      <div class="wrapper">
        <div class="wrapper-text">Number of website visitors</div>
        <div id="visitor-count" class="data"></div>
      </div>
      <div class="wrapper">
        <div class="wrapper-text">Number of app downloads</div>
        <div id="download-count" class="data"></div>
      </div>
    </center>
  </body>
</html>
