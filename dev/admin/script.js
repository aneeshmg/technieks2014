$(document).ready(function() {
  console.log("jQuery loaded");
  $.get('../android/data/update.txt', function(data) {
    $('#updates').append(data);
  });
  $.get('../data/tweetofday.txt', function(data) {
    $('#tweets').append(data);
  });
  $.get('../data/visitcount.txt', function(data) {
    $('#visitor-count').append(data);
  });
  $.get('../android/data/downloadCount.txt', function(data) {
    $('#download-count').append(data);
  });
  $('#new-update').click(function() {
    var UpdateText = $('#updates').val();
    $.ajax( {
      url     : "../android/update.php",
      type    : "POST",
      data    : {
        "new"   :   UpdateText
      },
      success : function(response) {
        $('#updates').append(response);
      }
    });
  });
  $('#new-tweet').click(function() {
    var UpdateTweet = $('#tweets').val();
    $.ajax( {
      url     : "../update-tweet.php",
      type    : "POST",
      data    : {
        "new"   :   UpdateTweet
      },
      success : function(response) {
        $('#tweets').append(response);
      }
    });
  });
});
