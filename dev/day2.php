<?php
include 'visitorcounter.php';

function ActorCarousel() {
  echo "<div class=\"actors\"
  style=\"position:absolute;top:20px;left:30px;opacity:.9;background-color:#000;\">";
  for($i = 1; $i <= 10; $i++) {
    echo "<img src=\"images/gal" . (mt_rand(100,1000)) % 18 . ".jpg\" />";
  }
  echo "</div>";
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>techNIEks 2014 | Day 1</title>
      <meta charset="utf-8" />
      <meta name="Description" content="National level technocultural fest">
      <meta name="Keywords" content="youth, fest, annual, technocultural">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <link rel="shortcut icon" href="images/logo.png" type="image/png">
      <!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
      <script src="js/jquery.js" type="text/javascript"></script>
      <script>window.jQuery || document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><\/script>')</script>
      <script type="text/javascript" src="js/jquery.ui.core.min.js"></script>
      <script type="text/javascript" src="js/jquery.ui.widget.min.js"></script>
      <script type="text/javascript" src="js/jquery.ui.rcarousel.min.js"></script>
      <script>
        $(function() {
          $('.actors').rcarousel( {
            orientation: "vertical",
            auto:  {
              enabled: true,
              interval: 0
            },
            width:115,
            height:100,
            visible:6,
            speed:10000,
            step:1
          });
        });
      </script>
    <style>
      @import url(http://fonts.googleapis.com/css?family=Text+Me+One);
      @import url(http://fonts.googleapis.com/css?family=Varela+Round);
      body{
        background-image: url('images/bg.jpg');
      }
      #header {
        position: fixed center;
        height: 80px;
        margin: auto;
        width: 900px;
        padding-top: 10px;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
      }
      #logo-img {
        height: 70px;
        width: 60px;
        position: relative;
        top: -15px;
        right:290px;
      }
      #header-technieks {
        color: #fff;
        position: relative;
        top: -115px;
        font-family: 'Text Me One', sans-serif;
      }
      #header-technieks img {
        position: relative;
        top:0px;
      }
      #header-technieks b {
        color: #f00;
        font-size: 18px;
      }
      #header-colg {
        position: relative;
        bottom:95px;
        font-family: 'Text Me One', sans-serif;
        color: #fff;
        font-size: 20px;
      }
	  .eventBtn {
	    padding: 5px 15px 5px 15px;
		  font-size: 20px;
		  opacity: 0.9;
		  transition: .2s;
		  text-decoration: none;
		  position: relative;
		  top: 90px;
		  width: 100px;
		  font-family: 'text me one', 'sans serif';
		  font-weight: bold;
		  margin-left: 35px;
		  display: inline;
	  }
	  .ethnic {
	    color: #122A0A;
		  background-color: #FF8000;
	  }
	  .ethnic:hover {
	    color: #122A0A;
		  background-color: #FF8000;
		  opacity: 1;
		  text-decoration: none;
		  transition: 0.2s;
	  }
	  a:link {
	    text-decoration: none;
		  color: inherit;
	  }
	  a:hover {
	    text-decoration: none;
	  }
	  .eventBox1{
	    float: left;
		  margin: 10px 10px 10px 10px;
      font-family: 'Text Me One', 'Sans serif';
      color: #FF8000;
		  position: relative;
		  left: 12%;
		  right: 20%;
		  top: 110px;
		  overflow: hidden;
		  width: 300px;
		  height: 200px;;
		  opacity: 0.9;
		  font-weight: bold;
		  padding: 5px 10px 5px 10px;
    }
	  .color1 {
      background-color: #2A0A12;
	  }
	  .color2 {
      background-color: #0B0B61;
	  }
	  .eventBox1:hover {
	    opacity: 1;
		  transition: 0.2s;
		  cursor: pointer;
	  }
	  #day {
	    font-weight: bold;
		  color: #FFBF00;
		  position: relative;
		  top: 110px;
		  font-family: 'Varela Round', 'sans serif';
	  }
    </style>
  </head>
  <body>
    <center>
    <div id="header">
      <img src="images/logo.png" id="logo-img"/>
      <h2 id="header-colg">The National Institute of Engineering, Mysore presents</h2>
      <div id="header-technieks">
        <img src="images/techlogo2.png" height="70px" width="370px"/><br>
        "Celebrating 100 years of Indian cinema"<br><b>13th, 14th and 15th March</b>
      </div>
    </div>
    </center>
	  <center>
	    <div class = "eventBtn ethnic" style = "cursor: pointer"><a href = "day1.php">Day 1</a></div>
	    <div class = "eventBtn ethnic" style = "cursor: pointer"><a href = "day3.php">Day 3</a></div>
	    <br><br>
	    <div id = "day">DAY TWO EVENTS</div>
	    <br>
		
	    <div class = "eventBox1 color1"><b><!--a href = fblink-->Fifa<!--/a--></b><br>
	      Football on PC.</br>Details: At 10:30 am in GJB Room 204.</br>Fee: Rs 50
	    </div>
	    <div class = "eventBox1 color2"><b><!--a href = fblink-->Indian Movie Quiz<!--/a--></b><br>
	       Movie Based Quiz.</br>Details: At 10:30 am in Sir M V Hall.</br>Fee: Rs 30
	    </div>
	    <div class = "eventBox1 color1"><b><!--a href = fblink-->Maze Challenge<!--/a--></b><br>
	      Solve the mystery of puzzle.</br>Details: At 11:00 am in GJB Room 212.</br>Fee: Rs 20
	    </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Ulta Chess<!--/a--></b><br>
	    Crack your brains in loosing the game.</br>Details: At 11:00 am in GJB Room 107.</br>Fee: Rs 20
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Battle Field<!--/a--></b><br>
	    Virtual Shooting game.</br>Details: At 11:30 am in GJB 3rd floor.</br>Fee: Rs 60 per person and Rs 240 for a team of 4.
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Flappy Bird<!--/a--></b><br>
	    Test your patience with a frustrating and yet an addictive bird game.</br>Details: At 11:30 am in GJB Classroom.</br>Fee: Rs 20
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Carrom Mania<!--/a--></b><br>
	    Classic game of carrom.</br>Details: At 11:30 am in Sports Complex.</br>Fee: Rs 40
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Chowkabara<!--/a--></b><br>
	    Classical traditional game.</br>Details: At 12:00 pm in Canteen Hut.</br>Fee: Rs 10
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Image Chef<!--/a--></b><br>
	    Are you an expert at editing?</br>Details: At 12:00 pm in Canteen Hut.</br>Fee: Rs 20
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Spellathon<!--/a--></b><br>
	    TechNIEks version of spelling bee.</br>Details: At 12:30 pm in GJB Room 206.</br>Fee: Rs 20
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Entertainment Quiz<!--/a--></b><br>
	    How's your entertainment quotient?</br>Details: At 02:00 pm in Sir M V Hall.</br>Fee: Rs 30
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Eye Fight<!--/a--></b><br>
	    </br>Details: At 02:00 pm in GJB Room 215.</br>Fee: Rs 10
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Fastest Finger First<!--/a--></b><br>
	    Think your fingers can outrun Usain Bolt...Yes? Then let your fingers race to victory.</br>Details: At 02:00 pm in GJB Room 212.</br>Fee: Rs 30
	  </div>
	  
	  
	  </center>

    <?  ActorCarousel(); ?>
  </body>
</html>
