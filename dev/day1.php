<?php
include 'visitorcounter.php';

function ActorCarousel() {
  echo "<div class=\"actors\"
  style=\"position:absolute;top:20px;left:30px;opacity:.9;background-color:#000;\">";
  for($i = 1; $i <= 10; $i++) {
    echo "<img src=\"images/gal" . (mt_rand(100,1000)) % 18 . ".jpg\" />";
  }
  echo "</div>";
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>techNIEks 2014 | Day 1</title>
      <meta charset="utf-8" />
      <meta name="Description" content="National level technocultural fest">
      <meta name="Keywords" content="youth, fest, annual, technocultural">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <link rel="shortcut icon" href="images/logo.png" type="image/png">
      <!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
      <script src="js/jquery.js" type="text/javascript"></script>
      <script>window.jQuery || document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><\/script>')</script>
      <script type="text/javascript" src="js/jquery.ui.core.min.js"></script>
      <script type="text/javascript" src="js/jquery.ui.widget.min.js"></script>
      <script type="text/javascript" src="js/jquery.ui.rcarousel.min.js"></script>
      <script>
        $(function() {
          $('.actors').rcarousel( {
            orientation: "vertical",
            auto:  {
              enabled: true,
              interval: 0
            },
            width:115,
            height:100,
            visible:6,
            speed:10000,
            step:1
          });
        });
      </script>
    <style>
      @import url(http://fonts.googleapis.com/css?family=Text+Me+One);
      @import url(http://fonts.googleapis.com/css?family=Varela+Round);
      body{
        background-image: url('images/bgpink.jpg');
      }
      #header {
        position: fixed center;
        height: 80px;
        margin: auto;
        width: 900px;
        padding-top: 10px;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
      }
      #logo-img {
        height: 70px;
        width: 60px;
        position: relative;
        top: -15px;
        right:290px;
      }
      #header-technieks {
        color: #fff;
        position: relative;
        top: -115px;
        font-family: 'Text Me One', sans-serif;
      }
      #header-technieks img {
        position: relative;
        top:0px;
      }
      #header-technieks b {
        color: #f00;
        font-size: 18px;
      }
      #header-colg {
        position: relative;
        bottom:95px;  
        font-family: 'Text Me One', sans-serif;
        color: #fff;
        font-size: 20px;
      }
	  .eventBtn {
	    padding: 5px 15px 5px 15px;
		  font-size: 20px;
		  opacity: 0.9;
		  transition: .2s;
		  text-decoration: none;
	  	position: relative;
		  top: 90px;
		  width: 100px;
		  font-family: 'text me one', 'sans serif';
		  font-weight: bold;
		  margin-left: 35px;
		  display: inline;
	  }
	  .funky {
	    color: #000000;
		  background-color: #2E2EFE;
	  }
	  .funky:hover {
	    color: #01DFD7;
		  background-color: #000000;
		  opacity: 1;
		  text-decoration: none;
		  transition: 0.2s;
	  }
	  a:link {
	    text-decoration: none;
		  color: inherit;
	  }
	  a:hover {
	    text-decoration: none;
	  }
	  .eventBox1{
	    float: left;
		  margin: 10px 10px 10px 10px;
      font-family: 'Text Me One', 'Sans serif';
      color: #08088A;
		  position: relative;
		  left: 12%;
		  right: 20%;
		  top: 110px;
		  overflow: hidden;
		  width: 300px;
		  height: 200px;;
		  opacity: 0.9;
		  font-weight: bold;
		  padding: 5px 10px 5px 10px;
    }
	  .color1 {
      background-color: #CC2EFA;
	  }
	  .color2 {
      background-color: #FF00BF;
	  }
	  .eventBox1:hover {
	    opacity: 1;
		  transition: 0.2s;
		  cursor: pointer;
	  }
	  #day {
	    font-weight: bold;
		  color: #FF0040;
		  position: relative;
		  top: 110px;
		  font-family: 'Varela Round', 'sans serif';
	  }
    </style>
  </head>
  <body>
    <center>
    <div id="header">
      <img src="images/logo.png" id="logo-img"/>
      <h2 id="header-colg">The National Institute of Engineering, Mysore presents</h2>
      <div id="header-technieks">
        <img src="images/techlogo2.png" height="70px" width="370px"/><br>
        "Celebrating 100 years of Indian cinema"<br><b>13th, 14th and 15th March</b>
      </div>
    </div>
    </center>
	<center>
	  <div class = "eventBtn funky" style = "cursor: pointer"><a href = "day2.php">Day 2</a></div>
	  <div class = "eventBtn funky" style = "cursor: pointer"><a href = "day3.php">Day 3</a></div>
	  <br><br>
	  <div id = "day">DAY ONE EVENTS</div>
	  <br>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Counter Strike<!--/a--></b></p>
	    Computer game.</br>Details: At 10:30 am in CS Lab.</br>Fee: </br>Round 1 - Rs 200</br>Round 2 - Rs
      300</br>Round 3 - Rs 250
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Battle field<!--/a--></b></p>
	    Virtual Shooting game.</br>Details: At 11:30 am on the third floor.</br>Fee: Rs 60 per
      head - Team of 4
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->NFS<!--/a--></b></p>
	    Need For speed - Most Wanted.</br>Details: At 10:30 am in IS Lab.</br>Fee: Rs 30
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->FIFA<!--/a--></b></p>
	    Football on PC.</br>Details: At 10:30 am in GJB Room 204.</br>Fee: Rs 50
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Logo Quiz<!--/a--></b></p>
	    Identification of brands.</br>Details: At 11:00 am in Azeez Sait Auditorium.</br>Fee: Rs 30
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Bowling<!--/a--></b></p>
	    Hit the pins by rolling the ball.</br>Details: At 11:00 am in Basketball Court.</br>Fee: Rs 20
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->NIE Roadies<!--/a--></b></p>
	    Become the Roadie of NIE!</br>Details: At 11:30 am in GJB grounds.</br>Fee: Rs 120
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Tom, Dick and Harry<!--/a--></b></p>
	    Do you have the questioning ability?</br>Details: At 11:30 am in GJB Room 206 and
      207.</br>Fee: Rs 30 for team of two
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Mountain Dew<!--/a--></b></p>
	    Bottoms up with mountain dew.</br>Details: At 12:00 pm in parking lot.</br>Fee: Rs 30 per person and 10 off for Pair.
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Chowkabara<!--/a--></b></p>
	    Classical traditional game.</br>Details: At 12:00 pm in Canteen Hut.</br>Fee: Rs 10
	  </div>
	  <div class = "eventBox1 color1"><b><!--a href = fblink-->Brain Strain<!--/a--></b></p>
	    Strain your brain by participating in few series of activities.</br>Details: At 12:30 pm in GJB Room 212.
		</br>Fee: Rs 30
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Khoj<!--/a--></b></p>
	    College level treasure hunt.</br>Details: At 12:00 pm in GJB.</br>Fee: Rs 60
	   </div>
	   <div class = "eventBox1 color1"><b><!--a href = fblink-->Minute To Win It<!--/a--></b></p>
	    Become a winner in a minute.</br>Details: At 11:00 am in Parking Lot.</br>Fee: Rs 30
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Three Legged Race<!--/a--></b></p>
	    Team up with your partner and race till the end.</br>Details: At 02:00 pm in parking lot.</br>Fee: Rs 50
		</div>
		<div class = "eventBox1 color1"><b><!--a href = fblink-->Auto Quiz<!--/a--></b></p>
	    Test your knowledge on Automobiles.</br>Details: At 02:00 pm in Sir M V Hall .</br>Fee: Rs 30
	  </div>
	  <div class = "eventBox1 color2"><b><!--a href = fblink-->Mehendi<!--/a--></b></p>
	    How good are you at applying mehendi?</br>Details: At 02:00 pm in GJB Room 210.</br>Fee: Rs 20
	  </div>
	</center>
    <? ActorCarousel(); ?>
  </body>
</html>
