<?php
include "../visitorcounter.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <title>techNIEks 2014 | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/jquery.sliding_menu.css">
    <link  href="http://fotorama.s3.amazonaws.com/4.4.8/fotorama.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/mob-style.css">
    <script src="../js/jquery.js"></script>
    <script src="js/jquery.sliding_menu.js"></script>
    <script src="js/mob-script.js"></script>
  </head>
  <body>
    <div id="menu" style="display:none;">
      <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.html">About</a></li>
        <li><a href="contact.html">Contact</a></li>
        <li><a href="events.html">Events</a></li>
        <li><a href="sponsors.html">Sponsors</a></li>
        <li><a href="gallery.php">Gallery</a></li>
      </ul>
    </div>
    <div id="header">
      The National Institute of Engineering presents,
      <center><img src="../images/techlogo3.png" height="15%" width="70%"><br>
      <b>13th, 14th and 15th March</b></center>
    </div>
    <div class="content">
      <b>What to look out for:</b>
      <div class="fotorama" data-allowfullscreen="true" data-shuffle="false" style="margin-top: 7%;">
        <a href="../images/up1.jpg"></a>
        <a href="../images/up2.jpg"></a>
        <a href="../images/up3.jpg"></a>
        <a href="../images/up4.jpg"></a>
        <a href="../images/up5.jpg"></a>
      </div>
      <div id="updates">
      </div>
    </div>
  </body>
  <script src="http://fotorama.s3.amazonaws.com/4.4.8/fotorama.js"></script>
</html>
